import { Entity, model, property } from '@loopback/repository';
import { Skill } from './project.model';


@model({ settings: { strict: false } })
export class UserInfo extends Entity {
  @property({
    type: 'string',
    id: true,
    mongodb: { dataType: 'ObjectId' }
  })
  id?: string;

  @property({
    type: 'string'
  })
  firstName?: string;

  @property({
    type: 'string'
  })
  lastName?: string;

  @property({
    type: 'string'
  })
  position?: string;

  @property({
    type: 'string'
  })
  email?: string;

  @property({
    type: 'string'
  })
  phone?: string;

  @property({
    type: 'array',
    itemType: 'object'
  })
  skillsSummary?: Skill[];

  @property({
    type: 'array',
    itemType: 'string'
  })
  professionalProfile?: string[];

  @property({
    type: 'string'
  })
  linkedInProfile?: string;

  @property({
    type: 'string'
  })
  skypeName?: string;

  @property({
    type: 'string'
  })
  sourceCodeLink?: string;

  @property({
    type: 'string'
  })
  cvFileUrl?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<UserInfo>) {
    super(data);
  }
}

export interface UserInfoRelations {
  // describe navigational properties here
}

export type UserInfoWithRelations = UserInfo & UserInfoRelations;
