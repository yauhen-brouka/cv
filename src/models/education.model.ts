import { Entity, model, property } from '@loopback/repository';

@model({ settings: { strict: false } })
export class Education extends Entity {
  @property({
    type: 'string'
  })
  institution?: string;

  @property({
    type: 'string'
  })
  faculty?: string;

  @property({
    type: 'string'
  })
  speciality?: string;

  @property({
    type: 'string',
    id: true,
    mongodb: { dataType: 'ObjectId' }
  })
  id?: string;

  @property({
    type: 'string',
    mongodb: { dataType: 'ObjectId' }
  })
  userId?: string;

  @property({
    type: 'string'
  })
  degree?: string;

  @property({
    type: 'date'
  })
  fromDate?: string;

  @property({
    type: 'date'
  })
  toDate?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Education>) {
    super(data);
  }
}

export interface EducationRelations {
  // describe navigational properties here
}

export type EducationWithRelations = Education & EducationRelations;
