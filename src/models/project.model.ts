import { Entity, model, property } from '@loopback/repository';

enum SkillTypes {
  PROGRAMMING_LANGUAGES = 'Programming Languages',
  WEB_TECHNOLOGIES = 'Web Technologies',
  LIBRARIES = 'Libraries',
  CODE_QUALITY_TOOLS = 'Code Quality Tools',
  TESTING_TOOLS = 'Testing Tools',
  BUILD_TOOLS = 'Build Tools',
  APP_SERVER = 'Application Servers',
  OPERATING_SYSTEMS = 'Operating Systems',
  SOFTWARE = 'Software',
  VCS = 'Version Control Systems',
  DB = 'Databases',
  FOREIGN_LANGUAGES = 'Foreign Languages'
}

@model({settings: {strict: false}})
export class Skill extends Entity {
  @property({
    type: 'string'
  })
  name?: string;

  @property({
    type: 'string'
  })
  type?: SkillTypes;
}

@model()
export class Project extends Entity {
  @property({
    type: 'string',
    id: true,
    mongodb: { dataType: 'ObjectId' }
  })
  id?: string;

  @property({
    type: 'string',
    mongodb: { dataType: 'ObjectId' }
  })
  userId?: string;

  @property({
    type: 'string'
  })
  name?: string;

  @property({
    type: 'string'
  })
  role?: string;

  @property({
    type: 'string'
  })
  summary?: string;

  @property({
    type: 'date'
  })
  fromDate?: string;

  @property({
    type: 'date'
  })
  toDate?: string;

  @property({
    type: 'string'
  })
  region?: string;

  @property({
    type: 'string'
  })
  responsibilities?: string;

  @property({
    type: 'array',
    itemType: 'object'
  })
  skills?: Skill[];

  @property({
    type: 'string'
  })
  organization?: string;


  constructor(data?: Partial<Project>) {
    super(data);
  }
}

export interface ProjectRelations {
  // describe navigational properties here
}

export type ProjectWithRelations = Project & ProjectRelations;
