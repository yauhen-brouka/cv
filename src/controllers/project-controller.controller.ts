import {
  Filter,
  FilterExcludingWhere,
  repository,
} from '@loopback/repository';

import {
  param,
  get,
  getModelSchemaRef,
  response
} from '@loopback/rest';

import { uniqBy } from 'lodash';
import { Project, Skill } from '../models';
import { ProjectRepository } from '../repositories';

export class ProjectControllerController {
  constructor(
    @repository(ProjectRepository)
    public projectRepository: ProjectRepository
  ) {
  }

  @get('/project-items')
  @response(200, {
    description: 'Array of Project model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Project, { includeRelations: true })
        }
      }
    }
  })
  async find(
    @param.filter(Project) filter?: Filter<Project>
  ): Promise<Project[]> {
    return this.projectRepository.find(filter);
  }

  @get('/skills-summary')
  @response(200, {
    description: 'Array of Skills',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Skill, { includeRelations: false })
        }
      }
    }
  })
  async findAllSkills(
    @param.filter(Project) filter?: Filter<Project>
  ): Promise<Skill[]> {
    const projects = await this.projectRepository.find(filter);
    let skills: Array<Skill> = [];
    projects.forEach(project => {
      skills = skills.concat(project.skills as Array<Skill>);
    });

    return uniqBy(skills, 'name');
  }

  @get('/project-items/{id}')
  @response(200, {
    description: 'Project model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Project, { includeRelations: true })
      }
    }
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(Project, { exclude: 'where' }) filter?: FilterExcludingWhere<Project>
  ): Promise<Project> {
    return this.projectRepository.findById(id, filter);
  }
}
