import {
  Filter,
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
  response
} from '@loopback/rest';
import { UserInfo } from '../models';
import { UserInfoRepository } from '../repositories';

export class UserInfoControllerController {
  constructor(
    @repository(UserInfoRepository)
    public userInfoRepository: UserInfoRepository
  ) {
  }

  @get('/user-info')
  @response(200, {
    description: 'Array of UserInfo model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(UserInfo, { includeRelations: true })
        }
      }
    }
  })
  async find(
    @param.filter(UserInfo) filter?: Filter<UserInfo>
  ): Promise<UserInfo[]> {
    return this.userInfoRepository.find(filter);
  }
}
