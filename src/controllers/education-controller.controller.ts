import {
  Filter,
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
  response
} from '@loopback/rest';
import { Education } from '../models';
import { EducationRepository } from '../repositories';

export class EducationControllerController {
  constructor(
    @repository(EducationRepository)
    public educationRepository: EducationRepository
  ) {
  }


  @get('/educations')
  @response(200, {
    description: 'Array of Education model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Education, { includeRelations: true })
        }
      }
    }
  })
  async find(
    @param.filter(Education) filter?: Filter<Education>
  ): Promise<Education[]> {
    return this.educationRepository.find(filter);
  }
}
