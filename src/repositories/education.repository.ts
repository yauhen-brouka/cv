import { inject } from '@loopback/core';
import { DefaultCrudRepository } from '@loopback/repository';
import { MongoDataSource } from '../datasources';
import { Education, EducationRelations } from '../models';

export class EducationRepository extends DefaultCrudRepository<Education,
  typeof Education.prototype.id,
  EducationRelations> {
  constructor(
    @inject('datasources.mongo') dataSource: MongoDataSource
  ) {
    super(Education, dataSource);
  }
}
