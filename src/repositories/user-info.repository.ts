import { inject } from '@loopback/core';
import { DefaultCrudRepository } from '@loopback/repository';
import { MongoDataSource } from '../datasources';
import { UserInfo, UserInfoRelations } from '../models';

export class UserInfoRepository extends DefaultCrudRepository<UserInfo,
  typeof UserInfo.prototype.id,
  UserInfoRelations> {
  constructor(
    @inject('datasources.mongo') dataSource: MongoDataSource
  ) {
    super(UserInfo, dataSource);
  }
}
