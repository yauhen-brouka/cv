import { inject, lifeCycleObserver, LifeCycleObserver } from '@loopback/core';
import { juggler } from '@loopback/repository';
import { load } from 'ts-dotenv';

const env = load({
  MONGO_USER: String,
  MONGO_PASSWORD: String,
  MONGO_DB_NAME: String
});

const config = {
  name: 'mongo',
  connector: 'mongodb',
  url: `mongodb+srv://${env.MONGO_USER}:${env.MONGO_PASSWORD}@cvcluster.e2igz.mongodb.net/${env.MONGO_DB_NAME}?retryWrites=true&w=majority`,
  useNewUrlParser: true
};


// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
@lifeCycleObserver('datasource')
export class MongoDataSource extends juggler.DataSource
  implements LifeCycleObserver {
  static dataSourceName = 'mongo';
  static readonly defaultConfig = config;

  constructor(
    @inject('datasources.config.mongo', { optional: true })
      dsConfig: object = config
  ) {
    super(dsConfig);
  }
}
