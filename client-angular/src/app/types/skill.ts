import { SkillTypes } from './skill-types';

export type Skill = {
  name: string;
  type: SkillTypes;
}
