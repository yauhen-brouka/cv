import { UserInfo } from './user-info';

export const UserInfoDefaultValue: UserInfo = {
  firstName: '',
  lastName: '',
  skypeName: '',
  sourceCodeLink: '',
  skillsSummary: [],
  position: '',
  professionalProfile: [],
  id: '',
  email: '',
  phone: '',
  linkedInProfile: '',
  cvFileUrl: ''
};
