import { SkillTypes } from './skill-types';

export type SkillGroup = {
  type: SkillTypes;
  items: Array<string>
}
