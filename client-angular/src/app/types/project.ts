import { Skill } from './skill';

export type Project = {
  id?: string;
  userId?: string;
  name?: string;
  role?: string;
  summary?: string;
  fromDate?: string;
  toDate?: string;
  region?: string;
  responsibilities?: string;
  skills?: Array<Skill>;
  organization?: string;
}
