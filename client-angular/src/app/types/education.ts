export type Education = {
  institution?: string;
  faculty?: string;
  speciality?: string;
  id?: string;
  userId?: string;
  degree?: string;
  fromDate?: string;
  toDate?: string;
}
