export type UserInfo = {
  id: string;
  firstName: string;
  lastName: string;
  position: string;
  email: string;
  phone: string;
  linkedInProfile: string;
  skypeName: string;
  sourceCodeLink: string;
  cvFileUrl: string;

  skillsSummary: Array<{ name: string; type: string; }>,

  professionalProfile: Array<string>,
};
