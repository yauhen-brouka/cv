export enum SkillTypes {
  PROGRAMMING_LANGUAGES = 'Programming Languages',
  WEB_TECHNOLOGIES = 'Web Technologies',
  LIBRARIES = 'Libraries',
  CODE_QUALITY_TOOLS = 'Code Quality Tools',
  TESTING_TOOLS = 'Testing Tools',
  BUILD_TOOLS = 'Build Tools',
  APP_SERVER = 'Application Servers',
  OPERATING_SYSTEMS = 'Operating Systems',
  SOFTWARE = 'Software',
  VCS = 'Version Control Systems',
  DB = 'Databases',
  FOREIGN_LANGUAGES = 'Foreign Languages'
}
