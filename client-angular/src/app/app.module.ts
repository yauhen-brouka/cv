import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppMaterialModule } from './modules/material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './modules/app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './screens/home/home.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ProjectsComponent } from './screens/projects/projects.component';
import { EducationComponent } from './screens/education/education.component';
import { ContactsComponent } from './screens/contacts/contacts.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { LayoutModule } from '@angular/cdk/layout';
import { FooterComponent } from './components/footer/footer.component';
import { ProfileSummaryComponent } from './screens/home/components/profile-summary/profile-summary.component';
import { TopProjectsComponent } from './screens/home/components/top-projects/top-projects.component';
import { SkillsSummaryComponent } from './screens/home/components/skills-summary/skills-summary.component';
import { ProjectCardComponent } from './screens/projects/components/project-card/project-card.component';
import { EducationCardComponent } from './screens/education/components/education-card/education-card.component';
import { ProjectItemComponent } from './screens/project-item/project-item.component';
import { LoadingComponent } from './components/loading/loading.component';
import { LoadingScreenInterceptor } from './components/loading/loading.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    ProjectsComponent,
    EducationComponent,
    ContactsComponent,
    FooterComponent,
    ProfileSummaryComponent,
    TopProjectsComponent,
    SkillsSummaryComponent,
    ProjectCardComponent,
    EducationCardComponent,
    ProjectItemComponent,
    LoadingComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    AppMaterialModule,
    HttpClientModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    LayoutModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoadingScreenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
