import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api-service/api.service';
import { UserInfo } from '../../types/user-info';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { UserInfoDefaultValue } from '../../types/user-info.default';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
export class ContactsComponent implements OnInit {
  userInfo: UserInfo = UserInfoDefaultValue;

  constructor(
    private apiService: ApiService,
    private sanitizer: DomSanitizer
  ) {
  }

  sanitize(url: string): SafeResourceUrl {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  ngOnInit(): void {
    this.apiService.getUserInfo()
      .subscribe(userInfo => this.userInfo = userInfo);
  }
}
