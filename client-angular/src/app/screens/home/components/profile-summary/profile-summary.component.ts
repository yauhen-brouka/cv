import { Component, Input, OnInit } from '@angular/core';
import { UserInfo } from '../../../../types/user-info';
import { UserInfoDefaultValue } from '../../../../types/user-info.default';

@Component({
  selector: 'app-profile-summary',
  templateUrl: './profile-summary.component.html',
  styleUrls: ['./profile-summary.component.css']
})
export class ProfileSummaryComponent implements OnInit {
  @Input() userInfo: UserInfo = UserInfoDefaultValue;

  constructor() {
  }

  ngOnInit(): void {
  }

}
