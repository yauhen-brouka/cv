import { Component, HostListener, Input, OnInit } from '@angular/core';
import { SkillGroup } from '../../../../types/skill-group';

enum Breakpoints {
  MOBILE,
  SCREEN
}

@Component({
  selector: 'app-skills-summary',
  templateUrl: './skills-summary.component.html',
  styleUrls: ['./skills-summary.component.css']
})
export class SkillsSummaryComponent implements OnInit {
  @Input() skills: Array<SkillGroup> = [];
  breakpoint: Breakpoints = Breakpoints.MOBILE;
  expandedElement: SkillGroup = {} as SkillGroup;

  constructor() {
  }

  get mobileBreakpointValue() {
    return Breakpoints.MOBILE;
  }

  get screenBreakpointValue() {
    return Breakpoints.SCREEN;
  }

  ngOnInit(): void {
    this.breakpoint = (window.innerWidth <= 768) ? Breakpoints.MOBILE : Breakpoints.SCREEN;
  }

  @HostListener('window:resize', ['$event'])
  // eslint-disable-next-line
  onResize(event?: any) {
    this.breakpoint = (event?.target?.innerWidth <= 768) ? Breakpoints.MOBILE : Breakpoints.SCREEN;
  }

}
