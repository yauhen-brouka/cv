import { Component, HostListener, Input, OnInit } from '@angular/core';
import { Project } from '../../../../types/project';
import { DatesService } from '../../../../services/dates/dates.service';

@Component({
  selector: 'app-top-projects',
  templateUrl: './top-projects.component.html',
  styleUrls: ['./top-projects.component.css']
})
export class TopProjectsComponent implements OnInit {
  @Input() topProjects: Array<Project> = [];
  summaryLength = 300;
  breakpoint = 1;

  constructor(
    private datesService: DatesService
  ) {
  }

  ngOnInit(): void {
    this.breakpoint = (window.innerWidth <= 768) ? 1 : 3;
  }

  @HostListener('window:resize', ['$event'])
  // eslint-disable-next-line
  onResize(event?: any) {
    this.breakpoint = (event?.target?.innerWidth <= 768) ? 1 : 3;
  }

  getTruncatedSummary(text?: string): string {
    const textFragment = text?.slice(0, this.summaryLength) ?? '';
    const lastSpaceIndex = textFragment.lastIndexOf(' ');
    return textFragment.slice(0, lastSpaceIndex) + '...';
  }

  getProjectDateString(project: Project): string {
    if (!project.fromDate) {
      return '';
    }

    return this.datesService.getProjectDates(project.fromDate, project.toDate)
  }
}
