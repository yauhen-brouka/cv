import { Component, OnInit } from '@angular/core';
import sortedUniq from 'lodash/sortedUniq';

import { ApiService } from '../../services/api-service/api.service';
import { UserInfoDefaultValue } from '../../types/user-info.default';
import { UserInfo } from '../../types/user-info';
import { Project } from '../../types/project';
import { Skill } from '../../types/skill';
import { SkillGroup } from '../../types/skill-group';
import { SkillTypes } from '../../types/skill-types';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  userInfo: UserInfo = UserInfoDefaultValue;
  topProjects: Array<Project> = [];
  private rawSkills: Array<Skill> = [];

  constructor(
    private apiService: ApiService
  ) {
  }

  private getTopProjects() {
    this.apiService.getRecentUserProjects(this.userInfo.id)
      .subscribe(projects => this.topProjects = projects);
  }

  private getSkills() {
    this.apiService.getUserSkills(this.userInfo.id)
      .subscribe(skills => {
        this.rawSkills = skills;
      });
  }

  get userSkills(): Array<SkillGroup> {
    const skills = this.rawSkills;
    const types = skills.map(({ type }) => type).sort();
    const result: Array<SkillGroup> = [];

    sortedUniq(types).forEach((type: SkillTypes) => {
      const values = skills.filter(skill => skill.type === type);
      const items = values.map(({ name }) => name).sort();
      result.push({
        type,
        items: sortedUniq(items) as Array<string>
      } as SkillGroup);
    });

    return result;
  }

  ngOnInit(): void {
    this.apiService.getUserInfo()
      .subscribe((userInfo: UserInfo) => {
        this.userInfo = userInfo;
        this.getTopProjects();
        this.getSkills();
      });
  }
}
