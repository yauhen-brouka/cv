import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import sortBy from 'lodash/sortBy';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../services/api-service/api.service';
import { DatesService } from '../../services/dates/dates.service';
import { Project } from '../../types/project';
import { Skill } from '../../types/skill';

@Component({
  selector: 'app-project-item',
  templateUrl: './project-item.component.html',
  styleUrls: ['./project-item.component.css']
})
export class ProjectItemComponent implements OnInit {
  project: Project = {};

  constructor(
    private apiService: ApiService,
    private route: ActivatedRoute,
    private datesService: DatesService,
    private location: Location
  ) {
  }

  ngOnInit(): void {
    this.getProjectInfo();
  }

  getProjectInfo(): void {
    const projectId = this.route.snapshot.paramMap.get('id');
    this.apiService.getProject(projectId)
      .subscribe((project: Project) => {
        project.skills = sortBy(project.skills, ['type', 'name']);
        this.project = project;
      });
  }

  getProjectDates(): string {
    if (!this.project.fromDate) {
      return '';
    }

    return this.datesService.getProjectDates(this.project.fromDate, this.project.toDate);
  }

  getHtmlFromText(text: string | null) {
    return text?.replace(/(\\r\\n|\r\n|\\r|\r|\\n|\n)/g, '<br>');
  }

  goBack() {
    this.location.back();
  }

}
