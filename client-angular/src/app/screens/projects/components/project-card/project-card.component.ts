import { Component, Input, OnInit } from '@angular/core';
import { Project } from '../../../../types/project';
import { DatesService } from '../../../../services/dates/dates.service';
import { SkillTypes } from '../../../../types/skill-types';

@Component({
  selector: 'app-project-card',
  templateUrl: './project-card.component.html',
  styleUrls: ['./project-card.component.css']
})
export class ProjectCardComponent implements OnInit {
  @Input() project: Project = {};
  randomSkills: Array<string> = [];

  constructor(
    private datesService: DatesService
  ) {
  }

  ngOnInit(): void {
    // adding a separate field needed since calling @getRandomSkills() in *ngFor freezes the browser
    this.randomSkills = this.getRandomSkills();
  }

  getProjectDates(): string {
    if (!this.project.fromDate) {
      return '';
    }

    return this.datesService.getProjectDates(this.project.fromDate, this.project.toDate);
  }

  get projectSummaryHtml() {
    return this.project.summary?.replace(/(\\r\\n|\\r|\\n)/g, '<br>');;
  }

  getRandomSkills(): Array<string> {
    const techs = this.project.skills?.filter(t => t.type !== SkillTypes.FOREIGN_LANGUAGES);
    return techs?.sort(() => 0.5 - Math.random())
      .slice(0, 5)
      .map(({ name }) => name) ?? [];
  }
}
