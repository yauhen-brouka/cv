import { Component, OnInit } from '@angular/core';

import { ApiService } from '../../services/api-service/api.service';
import { Project } from '../../types/project';
import { UserInfo } from '../../types/user-info';
import { UserInfoDefaultValue } from '../../types/user-info.default';


@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {
  userInfo: UserInfo = UserInfoDefaultValue;
  userProjects: Array<Project> = [];

  constructor(
    private apiService: ApiService
  ) { }

  getUserProjects() {
    this.apiService.getUserProjects(this.userInfo.id)
      .subscribe((userProjects: Array<Project>) => this.userProjects = userProjects);
  }

  ngOnInit(): void {
    this.apiService.getUserInfo()
      .subscribe((userInfo: UserInfo) => {
        this.userInfo = userInfo;
        this.getUserProjects();
      });
  }

}
