import { Component, OnInit } from '@angular/core';
import { Education } from '../../types/education';
import { ApiService } from '../../services/api-service/api.service';
import { UserInfo } from '../../types/user-info';
import { UserInfoDefaultValue } from '../../types/user-info.default';


@Component({
  selector: 'app-education',
  templateUrl: './education.component.html',
  styleUrls: ['./education.component.css']
})
export class EducationComponent implements OnInit {
  userInfo: UserInfo = UserInfoDefaultValue;
  educationList: Array<Education> = [];

  constructor(
    private apiService: ApiService
  ) {
  }

  getUserEducation() {
    this.apiService.getUserEducation(this.userInfo.id)
      .subscribe((educationList: Array<Education>) => this.educationList = educationList);
  }

  ngOnInit(): void {
    this.apiService.getUserInfo()
      .subscribe((userInfo: UserInfo) => {
        this.userInfo = userInfo;
        this.getUserEducation();
      });
  }

}
