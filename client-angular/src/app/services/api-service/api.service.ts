import { Injectable } from '@angular/core';
import { UserInfo } from '../../types/user-info';
import { Project } from '../../types/project';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { UserInfoDefaultValue } from '../../types/user-info.default';
import { Skill } from '../../types/skill';
import { Education } from '../../types/education';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private API_PREFIX = '';

  private api = {
    userInfo: `${this.API_PREFIX}/user-info`,

    userRecentProjects: (userId: string): string => {
      const filterObject = {
        limit: 3,
        where: { userId }
      };

      return encodeURI(`${this.API_PREFIX}/project-items?filter=${JSON.stringify(filterObject)}`);
    },

    userAllProjects: (userId: string): string => {
      const filterObject = {
        where: { userId },
        order: 'fromDate DESC'
      };

      return encodeURI(`${this.API_PREFIX}/project-items?filter=${JSON.stringify(filterObject)}`);
    },

    allSkills: (userId: string): string => {
      const filterObject = {
        where: { userId }
      }

      return encodeURI(`${this.API_PREFIX}/skills-summary?filter=${JSON.stringify(filterObject)}`);
    },

    userEducation: (userId: string): string => {
      const filterObject = {
        where: { userId },
        order: 'fromDate DESC'
      }

      return encodeURI(`${this.API_PREFIX}/educations?filter=${JSON.stringify(filterObject)}`);
    },

    project: (projectId: string): string => {
      return `${this.API_PREFIX}/project-items/${projectId}`;
    }
  };

  userInfo?: UserInfo;

  constructor(
    private http: HttpClient
  ) {
  }

  private handleError(error: unknown): Observable<UserInfo> {
    console.error(error);
    this.userInfo = UserInfoDefaultValue;
    return of(this.userInfo);
  }

  getUserInfo(): Observable<UserInfo> {
    return this.http.get<UserInfo[]>(this.api.userInfo)
      .pipe(
        map((response: UserInfo[]) => {
          this.userInfo = response[0];
          return this.userInfo;
        }),
        catchError(this.handleError)
      );
  }

  getRecentUserProjects(userId: string): Observable<Array<Project>> {
    if (!userId) {
      throw new Error('getRecentUserProjects: Missing User ID');
    }

    return this.http.get<Array<Project>>(this.api.userRecentProjects(userId));
  }

  getUserProjects(userId: string): Observable<Array<Project>> {
    if (!userId) {
      throw new Error('getUserProjects: Missing User ID');
    }

    return this.http.get<Array<Project>>(this.api.userAllProjects(userId));
  }

  getUserSkills(userId: string): Observable<Array<Skill>> {
    if (!userId) {
      throw new Error('getUserSkills: Missing User ID');
    }

    return this.http.get<Array<Skill>>(this.api.allSkills(userId));
  }

  getUserEducation(userId: string): Observable<Array<Education>> {
    if (!userId) {
      throw new Error('getUserEducation: Missing User ID');
    }

    return this.http.get<Array<Education>>(this.api.userEducation(userId));
  }

  getProject(projectId: string | null): Observable<Project> {
    if (!projectId) {
      throw new Error('getProject: Missing Project ID');
    }

    return this.http.get<Project>(this.api.project(projectId));
  }
}
