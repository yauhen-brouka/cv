import { Injectable } from '@angular/core';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class DatesService {

  constructor() { }

  getProjectDates(fromDate: string, toDate?: string) {
    let dateString = moment(fromDate).format('MMM YYYY') + ' - ';
    dateString += toDate ? moment(toDate).format('MMM YYYY') : 'Present';

    return dateString;
  }
}
