import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from '../screens/home/home.component';
import { ProjectsComponent } from '../screens/projects/projects.component';
import { EducationComponent } from '../screens/education/education.component';
import { ContactsComponent } from '../screens/contacts/contacts.component';
import { ProjectItemComponent } from '../screens/project-item/project-item.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'projects',
    component: ProjectsComponent
  },
  {
    path: 'education',
    component: EducationComponent
  },
  {
    path: 'contacts',
    component: ContactsComponent
  },
  {
    path: 'projects/:id',
    component: ProjectItemComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
