import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

type RouteItem = {
  label: string,
  path: string,
  icon: string
};

const RouteList: RouteItem[] = [
  {
    label: 'Home',
    icon: 'home',
    path: '/'
  },
  {
    label: 'Projects',
    icon: 'history',
    path: '/projects'
  },
  {
    label: 'Education',
    icon: 'school',
    path: '/education'
  },
  {
    label: 'Contacts',
    icon: 'call',
    path: '/contacts'
  }
];

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  activeRoute = '';
  routes = RouteList;

  constructor(
    private router: Router
  ) {
  }

  private subscribeForRouteChange(): void {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.activeRoute = event.url;
      }
    });
  }

  ngOnInit(): void {
    this.subscribeForRouteChange();
  }
}
