import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { LoadingScreenService } from './services/loading-screen/loading-screen.service';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  loading = false;
  loadingSubscription: Subscription | null = null;

  constructor(private loadingScreenService: LoadingScreenService) {
  }

  ngOnInit() {
    this.loadingSubscription = this.loadingScreenService.loadingStatus.pipe(
      debounceTime(100)
    ).subscribe((value => {
      this.loading = value;
    }));
  }

  ngOnDestroy() {
    this.loadingSubscription?.unsubscribe();
  }
}
