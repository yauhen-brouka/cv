# Check out https://hub.docker.com/_/node to select a new base image
# Building UI
FROM node:15.14.0-alpine3.10 as ui-build

USER node

RUN mkdir -p /home/node/cv-ui

WORKDIR /home/node/cv-ui

COPY --chown=node client-angular/package*.json /home/node/cv-ui/

RUN npm i

COPY  --chown=node client-angular /home/node/cv-ui

RUN ls -la

RUN npm run build


# Building Server
FROM node:15.14.0-alpine3.10
# Set to a non-root built-in user `node`
USER node

# Create app directory (with user `node`)
RUN mkdir -p /home/node/app

WORKDIR /home/node/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY --chown=node package*.json ./

RUN npm install

# Bundle app source code
COPY --chown=node . .

COPY --from=ui-build /home/node/cv-ui/dist /home/node/app/public

RUN npm run build

# Bind to all network interfaces so that it can be mapped to the host OS
ENV HOST=0.0.0.0 PORT=3000

EXPOSE ${PORT}
CMD [ "node", "." ]
